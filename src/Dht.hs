module Dht where

import Control.Monad.Free
import Control.Monad.State (modify')
import Control.Monad.Trans.Writer.Lazy (execWriterT)
import Data.Int
import Data.Map.Strict qualified as M
import Data.Text
import GHC.Exts
import Numeric.Natural
import Protolude

-- {{ Domain daatypes
type RoutingKey = Int8
type DataKey = RoutingKey
type NodeId = RoutingKey
type SenderNode = NodeId
type RecipientNode = NodeId

-- | Magic cookie aka correlation id
type Magic = Int64
-- }}

-- {{{ DHT functor definition and DHT operations
data DhtAction p next
  = GenMagic (Magic -> next)
  | Delay next
  | Listen (MsgEnvelope p -> next)
  | Send (MsgEnvelope p) next
  deriving (Functor)

data MsgEnvelope p = MsgEnvelope
  { magic :: Magic
  , sender :: SenderNode
  , recipient :: RecipientNode
  , payload :: Message p
  } deriving (Eq, Show)

data Message p
  = Ping
  | Store (DataKey, p)
  | FindNode NodeId
  | FindValue DataKey
  deriving (Eq, Show)

type DhtM p = Free (DhtAction p)

genMagic :: DhtM p Magic
genMagic = liftF $ GenMagic identity

delay :: DhtM p ()
delay = liftF $ Delay ()

send :: Magic -> SenderNode -> RecipientNode -> Message p -> DhtM p ()
send magic sender recipient msg = liftF $ Send (MsgEnvelope magic sender recipient msg) ()

listen :: DhtM p (MsgEnvelope p)
listen = liftF $ Listen identity
-- }}}

dhtNode :: NodeId -> DhtM p ()
dhtNode thisNodeId = do
  newMagic <- genMagic
  listen >>= \case
    MsgEnvelope{sender, magic, payload = Ping} -> send magic thisNodeId sender Ping
    _ -> pure ()

runNetwork :: [(NodeId, DhtM Text ())] -> [(Natural, SenderNode, Message Text)]
runNetwork nodes = execWriterT . (`evalState` NetworkState (fromList nodes) [] []) . forever $ do
  activeNodes <- takeActiveNodes

  forM_ (M.assocs activeNodes) $ \(nodeId, node) ->
    unwrapFree (interpretDht nodeId) node

  clearDanglingOutgoing
  where
    interpretDht :: MonadState (NetworkState Text) m => NodeId -> DhtAction Text (DhtM Text ()) -> m ()
    interpretDht nodeId = \case
      Send envelope next -> do
        addMsgToOutgoing envelope
        addToActive [(nodeId, next)]
      Listen f -> addToListening [(nodeId, f)]
      _ -> undefined

-- {{{ Network state handling
data NetworkState p = NetworkState
  { activeNodes :: Map NodeId (DhtM p ())
  , listeningNodes :: Map NodeId (MsgEnvelope p -> DhtM p ())
  , outgoingMessages :: Map RecipientNode [MsgEnvelope p]
  }

takeActiveNodes :: MonadState (NetworkState p) m => m (Map NodeId (DhtM p ()))
takeActiveNodes = state $ \ns@NetworkState{activeNodes} -> (activeNodes, ns{activeNodes=mempty})

takeListeningNodes :: MonadState (NetworkState p) m => m (Map NodeId (MsgEnvelope p -> DhtM p ()))
takeListeningNodes = state $ \ns@NetworkState{listeningNodes} -> (listeningNodes, ns{listeningNodes=mempty})

addToActive :: MonadState (NetworkState p) m => [(NodeId, DhtM p ())] -> m ()
addToActive nodes = modify' $ \ns@NetworkState{activeNodes} ->
  ns{activeNodes = activeNodes <> fromList nodes}

addToListening :: MonadState (NetworkState p) m => [(NodeId, MsgEnvelope p -> DhtM p ())] -> m ()
addToListening nodes = modify' $ \ns@NetworkState{listeningNodes} ->
  ns{listeningNodes = listeningNodes <> fromList nodes}

addMsgToOutgoing :: MonadState (NetworkState p) m => MsgEnvelope p -> m ()
addMsgToOutgoing envelope@MsgEnvelope{recipient} = modify' $ \ns@NetworkState{outgoingMessages} ->
  ns{outgoingMessages = M.adjust (<> [envelope]) recipient outgoingMessages}

-- | Remove messages from outgoing, which do not have any recipient in the network
clearDanglingOutgoing :: MonadState (NetworkState p) m => m ()
clearDanglingOutgoing = undefined
-- }}}

-- {{{ Free monad utils
-- | Unwrap a single layer of a free monad
unwrapFree :: Monad m => (f (Free f a) -> m a) -> Free f a -> m a
unwrapFree _ (Pure a) = pure a
unwrapFree f (Free as) = f as
-- }}}
